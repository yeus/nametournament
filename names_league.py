#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr 21 23:49:28 2020

#cypher utilities

@author: tom
"""

import functools  # for custom comparison function
import logging
import os
import random
import sqlite3

import numpy as np
import pandas as pd
import pygame
import sklearn as sk
import sklearn.base
import sklearn.feature_extraction.text
import sklearn.linear_model
import sklearn.neighbors
import sklearn.pipeline
import tqdm

# import torch
# import torch.nn as nn
# import torch.optim as optim
# import torch.nn.functional as F

logger = logging.getLogger(__name__)

all_name_features = """
SELECT * FROM names
"""

"""
def getch():
    def _getch():
        fd = sys.stdin.fileno()
        old_settings = termios.tcgetattr(fd)
        try:
            tty.setraw(fd)
            ch = sys.stdin.read(1)
        finally:
            termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
        return ch
    return _getch()
"""


def insertitems(itemdict, tablename, conn):
    # Insert a row of data
    itemkeys = ",".join(itemdict.keys())
    itemplaceholders = ",".join(["?"] * len(itemdict))
    sql = f"INSERT INTO '{tablename}' ({itemkeys}) VALUES ({itemplaceholders})"
    print(sql, itemdict)
    conn.execute(sql, tuple(itemdict.values()))
    conn.commit()
    return conn


# class namerank(nn.Module):
#     def __init__(self, D_in, BN=False):
#         super(DQN, self).__init__()
#         N, H, D_out = 32, 20, 3

#         self.fc1 = nn.Linear(D_in, H)
# #        self.fc2 = nn.Linear(H, H)
#         self.fc3 = nn.Linear(H, D_out)

#     # Called with either one element to determine next action, or a batch
#     # during optimization. Returns tensor([[left0exp,right0exp]...]).
#     def forward(self, x):
#         x = F.relu(self.fc1(x))
#         x = F.relu(self.fc2(x))
#         x = self.fc3(x)
#         return x

class nameranking:
    def __init__(self):
        db = "./babynames_USA_thomas.db"
        self.conn = sqlite3.connect(db)
        self.popularity = pop = pd.read_sql("""
            SELECT  year,
                    SUM(popularity),
                    year_name_id 
            FROM popularity
                WHERE year BETWEEN 2000 AND 2020
            GROUP BY year_name_id
              """,
                                            self.conn, index_col="year_name_id"
                                            ).sort_values(by='SUM(popularity)')
        self.insert_names = self.popularity.index.values.tolist()
        self.allnames = pd.read_sql(
            all_name_features,
            self.conn,
            index_col="name_id")

        self.table = self.allnames.index.values.tolist()
        self.batch = []
        self.model = self.build_model()

        # self.all_features = self.get_all_features()
        self.all_features = pd.read_parquet('./features.parquet')

        self.vecsize = 3000
        self.fit_vectorizer()
        self.sortids = []

        self.cycles = 0

    def fit_vectorizer(self):
        self.tfidfvec = tv = sklearn.feature_extraction.text.TfidfVectorizer(
            analyzer='char',
            ngram_range=(1, 4),
            max_df=0.9,
            max_features=self.vecsize)

        self.tfidfvec.fit(self.all_features.ipa)

    def get_vocabulary(self):
        chars = set()
        [chars.update(a) for a in self.all_features.ipa]
        return chars

    def get_name_by_id(self, name_id):
        n = self.conn.execute("""
               SELECT name,sex FROM names
               WHERE name_id = ?
               """, [name_id])
        return n.fetchall()[0]

    def get_next_names(self):
        # TODO this has to be a better strategy
        mode = "random"
        batchsize = 10
        shufflesize = 300
        if mode == "random":
            if len(self.batch) < 2:  # if batch is empty
                self.train()
                shuffle = self.sort_table(shufflesize, extend=False)
                self.batch = shuffle[:batchsize]
                self.cycles += 1
        else:
            if not self.sortids: self.sort_table(10)  # initialize
            if len(self.batch) < 2:  # if batch is empty
                self.train()
                shuffle = self.sort_table(20)[:shufflesize]
                top_threshold = 6 + self.cycles * 2
                top = random.sample(shuffle[:top_threshold], 6)
                self.batch = top + random.sample(shuffle[top_threshold:], batchsize)
                random.shuffle(self.batch)
                self.cycles += 1

        ids = (self.batch.pop(0), self.batch.pop(0))
        n1, s1 = self.get_name_by_id(ids[0])
        n2, s2 = self.get_name_by_id(ids[1])
        return (ids[0], n1, s1), (ids[1], n2, s2)

    def insert_duel_result(self, n1id, n2id, winner):
        winner = 0 if winner == "left" else 1
        insertitems({"name1": n1id, "name2": n2id,
                     "winner": winner},
                    "name_duels", self.conn)

    def insert_favorites(self, name_id):
        logger.info(f"saving favorite: {self.allnames.loc[name_id]}")
        insertitems({"name_id": n1id},
                    "favorites", self.conn)

    def get_all_features(self):
        features = pd.read_sql("""
              SELECT * FROM features_ipa_extended
              """,
                               self.conn, index_col='feature_name_id')
        return features

    def get_features(self, name_id):
        # features = pd.read_sql("""
        #       SELECT * FROM features_ipa_extended
        #       WHERE feature_name_id=?
        #       """,
        #       self.conn,
        #       params=(name_id,))
        features = self.all_features
        return features.loc[name_id]

    def vectorize(self, n_id):
        feat = self.get_features(n_id)
        vec = self.tfidfvec.transform([feat.ipa])
        f = np.hstack((
            feat.sex == 'M',
            len(self.allnames.loc[n_id]),
            self.popularity['SUM(popularity)'].get(n_id) or -1,
            vec.toarray().flatten()))
        return f

    def model_input(self, n1id, n2id):
        f1v = self.vectorize(n1id)
        f2v = self.vectorize(n2id)
        return np.hstack([f1v, f2v])

    def build_training_set(self):
        allduels = self.get_duels()

        X = []
        Y = []
        for idx, row in allduels.iterrows():
            n1id, n2id, winner = row
            X.append(self.model_input(n1id, n2id))
            Y.append(winner)

        return X, Y

    def get_duels(self):
        allduels = pd.read_sql("""
               SELECT name1,name2,winner FROM name_duels
               """,
                               self.conn)

        return allduels

    def build_model(self):
        classifier = sk.linear_model.LogisticRegression(
            max_iter=200,
            verbose=1,
            n_jobs=-1)

        # pipe = sk.pipeline.make_pipeline(
        #     skl.pipeline.make_union(
        #          vectorizer,
        #          vectorizer
        #         ),
        #     classifier
        #     )
        return classifier

    def testmodel(self, X_test, y_test):
        pipe = self.model
        est = pd.DataFrame(pipe.predict_proba(X_test), columns=pipe.classes_)
        est['max'] = est.loc[:, pipe.classes_].max(axis=1)  # get the calculated certainty
        est['maxidx'] = est.loc[:, pipe.classes_].idxmax(axis=1)  # get the label
        est['category'] = y_test  # get the "correct" category

        # Model Accuracy
        predicted, test = est[['maxidx', 'category']].values.T
        accuracy = sk.metrics.accuracy_score(test, predicted)
        print(f"Logistic Regression Accuracy: {accuracy}")
        print(sk.metrics.classification_report(test, predicted))

    def convert_to_names(self, table):
        tablen = self.allnames.loc[table]
        return tablen

    def get_name_ranks(self, n=1000, reset=False):
        if reset: self.sort_table(n, mode="popular",
                                  extend=False)
        self.sortnames = nr.convert_to_names(self.sortids)
        return self.sortnames

    def sort_table(self, nlist=100, extend=True,
                   mode="random"):
        process = tqdm.tqdm("sorting list ...")

        def predict_duel(n1id, n2id):
            x = self.model_input(n1id, n2id)
            winner = self.model.predict([x])
            winner = winner * 2 - 1
            process.update(1)
            return winner

        # self.table = sorted(self.table, key=functools.cmp_to_key(predict_duel))
        if mode == "random":
            newnames = random.sample(self.table, nlist)
        elif mode == "popular":
            newnames = self.popularity.index.values.tolist()[-nlist:]
        else:
            newnames = [self.insert_names.pop() for i in range(nlist)]
        if extend: newnames += self.sortids
        self.sortids = sorted(
            newnames,
            key=functools.cmp_to_key(predict_duel))
        return self.sortids

    def train(self):
        logger.info("start training")
        X, Y = self.build_training_set()
        X = np.array(X)
        Y = np.array(Y)

        (X_train, X_test,
         y_train, y_test) = sk.model_selection.train_test_split(
            X, Y, test_size=0.3, random_state=2)

        logger.info("test model...")
        self.model.fit(X_train, y_train)
        self.testmodel(X_test, y_test)


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    nr = nameranking()
    nr.train()

    game = True
    if game:
        width = 20

        os.environ['SDL_VIDEO_CENTERED'] = '1'
        pygame.init()
        clock = pygame.time.Clock()
        pygame.font.init()
        pygame.display.set_caption("Baby Name Duel")
        font = pygame.font.SysFont('Hack', 20)
        fontsmall = pygame.font.SysFont('ubuntu', 15)
        screen = pygame.display.set_mode((1200, 600))

        (n1id, n1, s1), (n2id, n2, s2) = nr.get_next_names()
        winner = "vs"

        hist = []


        def gprint(txt, pos):
            text = font.render(txt, 1, (255, 255, 255))
            screen.blit(text, pos)


        def render():
            global n1, n2, winner, s1, s2
            screen.fill((0, 0, 0))
            welcome = 'which one do you prefer?'
            gprint(welcome, (50, 5))
            names = f"{n1:>{width}} vs {n2:<{width}}"
            gprint(names, (5, 200))
            sexes = f"{s1:>{width}}    {s2:<{width}}"
            gprint(sexes, (5, 170))
            for i, (x1, x2, winner) in enumerate(reversed(hist[-10:])):
                if winner == "left":
                    f1 = int((1 - (i / 10)) * 255)
                    f2 = int(f1 // 2)
                    r = f2
                    res = "<<<"
                else:
                    f2 = int((1 - (i / 10)) * 255)
                    f1 = int(f2 // 2)
                    r = f1
                    res = ">>>"

                nr1 = f"{x1:>{width}}"
                nr2 = f"{x2:<{width}}"
                x = font.render(res, 1, (r, r, r))
                screen.blit(x, (650, 40 + (1 + i) * 50))
                x = font.render(nr1, 1, (f1, f1, f1))
                screen.blit(x, (400, 40 + (1 + i) * 50))
                x = font.render(nr2, 1, (f2, f2, f2))
                screen.blit(x, (700, 40 + (1 + i) * 50))

            # draw current rankings
            # nr.sort_table()
            ranks = nr.get_name_ranks()[:10]
            for i, row in enumerate(ranks.itertuples()):
                x = font.render(row.name, 1, (255, 200, 200))
                screen.blit(x, (900, 40 + (1 + i) * 50))

            pygame.display.flip()


        while True:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
                    exit()
                    running = False
                elif event.type == pygame.KEYDOWN:
                    if (event.key == pygame.K_ESCAPE or
                            event.key == pygame.K_q):
                        pygame.quit()
                        continue
                    elif event.key == pygame.K_RIGHT:
                        if pygame.key.get_mods() & pygame.KMOD_SHIFT:
                            nr.insert_favorites(n2id)
                            continue
                        else:
                            winner = "right"
                    elif event.key == pygame.K_LEFT:
                        if pygame.key.get_mods() & pygame.KMOD_SHIFT:
                            nr.insert_favorites(n1id)
                            continue
                        else:
                            winner = "left"
                    elif event.key == pygame.K_DOWN:
                        winner = "none"
                    elif event.key == pygame.K_i:
                        namelist = nr.get_name_ranks(reset=True)
                        print(namelist)
                    else:
                        continue

                    # game logic if key was pressed
                    while winner != "vs":
                        if not winner == "none":
                            duel = (n1, n2, winner)
                            hist += [duel]
                            nr.insert_duel_result(n1id, n2id, winner)
                        (n1id, n1, s1), (n2id, n2, s2) = nr.get_next_names()
                        if s1 == "M" and s2 == "F":
                            winner = "left"
                        elif s1 == "F" and s2 == "M":
                            winner = "right"
                        elif s1 == "F" and s2 == "F":
                            winner = "none"
                        else:
                            winner = "vs"
                        print(f"{s1, n1, winner, n2, s2}")

            # key = pygame.key.get_pressed()
            render()
            clock.tick(60)

    namelist = nr.get_name_ranks(reset=True)
