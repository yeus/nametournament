#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr 21 23:49:28 2020

#cypher utilities

@author: tom
"""
import functools
import logging
import os
import random
from functools import cached_property
from typing import List, Tuple

import diskcache as dc
import numpy as np
import pandas as pd
import pygame
import sklearn as sk
import sklearn.base
import sklearn.ensemble
import sklearn.feature_extraction.text
import sklearn.linear_model
import sklearn.neighbors
import sklearn.pipeline
import tqdm
from sklearn.feature_extraction.text import CountVectorizer

# import torch
# import torch.nn as nn
# import torch.optim as optim
# import torch.nn.functional as F

logger = logging.getLogger(__name__)

alphabet = "abcdefghijklmnopqrstuvwxyz"


def build_seed_word(start_word, word_index, max_length: int = 10, random_letter_prob: float = 0.0, force_chain=False):
    """builds a new word using the seed_word_index"""
    start = random.choice(start_word)
    min_length = 2
    word = start[:random.randint(min_length, max(min_length, len(start)))]

    for i in range(100):
        overlap = random.randint(1, 3)
        p2 = word_index.txt.get(word[-overlap:], "")

        if len(p2) > 0:
            syl = random.choice(p2)
            syl_start = min(overlap, len(syl))
            cutoff = random.randint(syl_start, max(syl_start, len(syl)))
            word += syl[overlap:cutoff] or ""
        elif force_chain:
            syl = random.choice(word_index.txt.sample()[0])
            cutoff = random.randint(1, max(len(syl)-1,1))
            word += syl[0:cutoff] or ""

        if len(word) > max_length:
            break

    return word


class nameranking:
    def __init__(self):
        syllables: pd.DataFrame = pd.read_json("syls.json")

        # the history saves every "match" ever played
        # the winner is declared by the string:  "<", ">" or "=" for
        # a tie
        self.history = dc.Cache('company_name_results')
        # TODO: do this with IPAs instead of just letters...
        self.favorites: List[str] = []

        self.batch: List[Tuple[str, str]] = []
        # self.model = self.build_model()

        self.max_features = 10000
        # self.sortids = []

        # self.cycles = 0
        self._sorted_names = None

    @cached_property
    def ignore_list(self):
        with open("ignore_list.txt") as f:
            ignore_list = f.read()

        return ignore_list.split()

    @cached_property
    def ignore_list_str(self):
        return " ".join(self.ignore_list)

    @cached_property
    def seed_text(self):
        with open("seed_text.md") as f:
            seed_text = f.read()

        ws = seed_text.lower().split()
        seed_text = ["".join(c for c in w if c in alphabet) for w in ws]
        seed_text = [t for t in seed_text if t]
        return seed_text

    @cached_property
    def inspirational_syllables(self):
        vectorizer = CountVectorizer(analyzer="char", ngram_range=(3, 5))
        vectorizer.fit(self.seed_text)
        return list(vectorizer.vocabulary_.keys())

    def word_index(self, words):
        df = pd.DataFrame([((w[:3], w[:2], w[:1]), w) for w in words], columns=["index", "txt"])
        df = df.drop_duplicates()
        word_index = df.explode('index').groupby('index').agg(list)
        return word_index

    @cached_property
    def seed_word_index(self):
        return self.word_index(self.seed_text)

    def build_seed_word(self, max_length: int = 10, random_letter_prob: float = 0.0):
        """builds a new word using the seed_word_index"""
        return build_seed_word(
            start_word=random.choice(self.seed_text),
            word_index=self.seed_word_index,
            max_length=max_length,
            random_letter_prob=random_letter_prob
        )

    def get_all_names(self):
        return list(set(n for names in list(self.history) for n in names))

    def fit_vectorizer(self):
        # TODO: consider using https://scikit-learn.org/stable/modules/generated/sklearn.feature_extraction.text.HashingVectorizer.html
        self.tfidfvec = sklearn.feature_extraction.text.TfidfVectorizer(
            analyzer='char',
            ngram_range=(1, 3),
            max_df=0.9,
            max_features=self.max_features)

        if all_names := self.get_all_names():
            logger.info("fit vectorize")
            self.tfidfvec.fit(all_names)
        else:
            logger.info("initial vectorizer fit")
            self.tfidfvec.fit(["".join(random.sample(alphabet, 10)) for i in range(10)])  # randomly assign word

    def generate_random_word(
            self,
            word_quality: int = 1,
            word_num: int = 1,
            syllable_source: str = "",
            add_random_letter: bool = False,
            recombine: float = 0,  # percentage of top ranked names
            method: str = "syllables"
    ) -> List[str]:
        top_name_num = 10
        wordlist = []
        # important_features = self.model_feature_importance().to_frame('score')
        # important_features['syl']= important_features.index.str[3:]
        # important_features = important_features.loc[~important_features['syl'].str.endswith("_")]
        # important_features = important_features.loc[important_features['syl'].str.len()>1]
        # important_syls = important_features[-10:].syl.to_list()

        # TODO: add a search-index method based on overlapping n-grams. we will take our favorite
        #       words, and recombine their ngrams based on overlaps "that work"

        if recombine:
            ranked_names = self.get_name_ranks()
            wordlist += ranked_names[-int(len(ranked_names) * recombine):]
            new_words = []
            split_words = []
            for w in wordlist:
                splitpos = random.randint(1, max(len(w), len(w)) - 1)
                split_words += [w[:splitpos], w[-splitpos:]]
            wordindex = self.word_index(split_words)
            for i in range(word_num):
                new_words.append(build_seed_word(
                    start_word=random.choice(split_words),
                    word_index=wordindex,
                    max_length=random.randint(6,12), force_chain=True
                ))
            return new_words

        # produce a string with all the top-words combined
        if wordlist and (syllable_source == "best"):
            ranked_names = self.get_name_ranks()
            wordlist += ranked_names[-top_name_num:]
            combined = "".join(wordlist)
            vectorizer = CountVectorizer(analyzer="char", ngram_range=(2, 5))
            vectorizer.fit([combined])
            syl_list = list(vectorizer.vocabulary_.keys())
        else:
            syl_list = self.inspirational_syllables

        choice_num = max(word_quality, word_num)
        if method == "syllables":
            syl_combis = [random.choices(syl_list, k=random.randint(2, 3)) for i in range(choice_num)]
            if add_random_letter:
                syl_combis = [s + random.choices(alphabet) for s in syl_combis]
                for s in syl_combis:
                    random.shuffle(s)
            new_words = ["".join(sc) for sc in syl_combis]
        else:  # elif method=="word_chain"
            new_words = [self.build_seed_word(max_length=random.randint(6, 12)) for i in range(choice_num)]

        sorted_words = self.sort_name_list(new_words)[::-1]

        return sorted_words[:word_num]

    def get_next_names(self) -> Tuple[str, str]:
        """
        always choose an already existing name...
        retrain for every new batch
        """
        # TODO: add some names where we need more information, because "predict_proba" is very imprecise
        if not self.batch:
            self.train()
            ranked_names = self.get_name_ranks(sample_size=100, cached=False)
            outcomes = self.count_word_outcomes()
            name_num = len(self.get_all_names())
            # top_winners = outcomes.index[-top_name_num:].tolist()
            logger.info(f"new batch generation... {outcomes.tail(10)}")
            if ranked_names:
                # winners_num = (self.batchsize // 10) * 3
                # set top matches
                # w1 = top_winners[-winners_num + 1::2][::-1]
                # w2 = top_winners[-winners_num::2][::-1]
                w1, w2 = [], []
                # add random words from actual top winners
                w1 += self.generate_random_word(word_quality=100, word_num=5, syllable_source="best")
                w2 += self.generate_random_word(word_quality=10, word_num=5)
                # use the 20% best ranked for recombinations...
                w1 += self.generate_random_word(word_num=5, recombine=0.2)
                w2 += self.generate_random_word(word_num=5, recombine=0.2, add_random_letter=True)
                w1 += self.generate_random_word(word_num=5, method="index_chain")
                w2 += self.generate_random_word(word_quality=20, word_num=5, method="index_chain")
                w1 += [ranked_names[-1]]
                w2 += [ranked_names[-2]]
                random.shuffle(w1), random.shuffle(w2)
                # top two names against each other
            else:
                w1 = self.generate_random_word(word_quality=1, word_num=5, method="index_chain")
                w2 = self.generate_random_word(word_quality=1, word_num=5, method="index_chain")

            # we randomly shuffle the names in order to prevent bias in the ranking algorithm
            self.batch = [(w1, w2) if random.random() > 0.5 else (w2, w1) for w1, w2 in zip(w1, w2)
                          if (not w1 == w2) and ((w1, w2) not in self.history) and ((w2, w1) not in self.history)]
            self.batch = [(w1, w2) for w1, w2 in self.batch if
                          ((w1 not in self.ignore_list_str) and (w2 not in self.ignore_list_str))]
            # random.shuffle(self.batch)
            logger.info("finished batch generation")

        return self.batch.pop()

    def insert_duel_result(self, n1: str, n2: str, result: str):
        self.history[(n1, n2)] = result

    def insert_favorites(self, name):
        self.favorites.append(name)

    def vectorize(self, name):
        vec = self.tfidfvec.transform([name])
        # caculate some additional metrics
        # for usage as a feature...
        f = np.hstack((
            len(name),
            vec.toarray().flatten()
        ))
        return f

    def get_model_input_feature_names(self):
        # Attention! if we change the vectorizer function we also have to change this here...
        features = ["len_"] + self.tfidfvec.get_feature_names_out().tolist()
        return [
            *["n1_" + n for n in features],
            *["n2_" + n for n in features]
        ]

    def model_input(self, name1, name2):
        f1v = self.vectorize(name1)
        f2v = self.vectorize(name2)
        return np.hstack([f1v, f2v])

    def build_training_set(self):
        allduels = list(self.history)

        X = []
        Y = []
        for n1, n2 in allduels:
            result = self.history[(n1, n2)]
            if result != "=":
                X.append(self.model_input(n1, n2))
                Y.append(result)
                # and for better training results also include the opposite:
                X.append(self.model_input(n2, n1))
                Y.append("<" if result == ">" else ">")

        X = np.array(X)
        Y = np.array(Y)
        return X, Y, self.get_model_input_feature_names()

    @cached_property
    def model(self):
        # try out different models here :)
        model_type = "random_forest"
        if model_type == "random_forest":
            classifier = sk.ensemble.RandomForestClassifier(
                n_estimators=100, max_depth=40
            )
        else:
            classifier = sk.linear_model.LogisticRegression(
                max_iter=200,
                verbose=1,
                n_jobs=-1
            )
        return classifier

    def testmodel(self, X_test, y_test):
        pipe = self.model
        est = pd.DataFrame(pipe.predict_proba(X_test), columns=pipe.classes_)
        est['max'] = est.loc[:, pipe.classes_].max(axis=1)  # get the calculated certainty
        est['maxidx'] = est.loc[:, pipe.classes_].idxmax(axis=1)  # get the label
        est['category'] = y_test  # get the "correct" category

        # Model Accuracy
        predicted, test = est[['maxidx', 'category']].values.T
        accuracy = sk.metrics.accuracy_score(test, predicted)
        logger.info(f"Model Accuracy: {accuracy}")
        print(sk.metrics.classification_report(test, predicted))

    def predict_duel(self, n1, n2):
        try:
            x = self.model_input(n1, n2)
            return self.model.predict([x])[0]
        except:
            return "="

    def get_all_duels(self):
        return pd.DataFrame(
            [(n1, n2, self.history[(n1, n2)]) for n1, n2 in self.history],
            columns=["name1", "name2", "result"]
        )

    def count_word_outcomes(self) -> pd.DataFrame:
        # TODO: use relation wins vs losses
        all_duels = self.get_all_duels()
        # remove all ties
        all_duels = all_duels.loc[all_duels.result != "="]
        # count wins & losses
        name_stats = {}

        for idx, row in all_duels.iterrows():
            n1stats = name_stats.get(row.name1, [0, 0])
            n2stats = name_stats.get(row.name2, [0, 0])
            if row.result == "<":
                n1stats[0] += 1
                n2stats[1] += 1
            elif row.result == ">":
                n1stats[1] += 1
                n2stats[0] += 1
            name_stats[row.name1] = n1stats
            name_stats[row.name2] = n2stats

        df = pd.DataFrame.from_dict(name_stats, orient="index", columns=["win", "loose"])
        df['win_ratio'] = (df.win) / (df.win + df.loose)
        df['win_lead'] = (df.win - df.loose) / (df.win + df.loose)
        df = df.sort_values(by=['win_ratio', 'win'])
        return df

    def sort_name_list(self, name_list) -> List[str]:
        process = tqdm.tqdm(desc="sorting list ...")

        def predict_duel(n1, n2):
            process.update(1)
            result = self.predict_duel(n1, n2)
            if result == "<":
                return 1
            elif result == ">":
                return -1
            else:
                return 0

        try:
            name_list = sorted(
                name_list,
                key=functools.cmp_to_key(predict_duel)
            )
            return name_list
        except sklearn.exceptions.NotFittedError:
            return []

    def get_name_ranks(self, sample_size=100, cached=True) -> List[str]:
        """return sorted list of names"""
        allnames = self.get_all_names()

        if not allnames:
            return []

        if cached and self._sorted_names:
            return self._sorted_names

        # self.table = sorted(self.table, key=functools.cmp_to_key(predict_duel))
        # TODO: reimplement better sorting methods than just random...
        #       we would also like to have the "complete" top100 for example...
        # if mode == "random":
        newnames = random.sample(allnames, min(sample_size, len(allnames)))

        self._sorted_names = self.sort_name_list(newnames)

        return self._sorted_names

    def train(self):
        logger.info("start training")
        self.fit_vectorizer()
        X, Y, feature_names = self.build_training_set()

        if len(X) > 2:
            (X_train, X_test,
             y_train, y_test) = sk.model_selection.train_test_split(
                X, Y, test_size=0.3, random_state=2)

            logger.info("test model...")
            self.model.fit(X_train, y_train)
            self.testmodel(X_test, y_test)
        else:
            logger.info("not enough training data")

    def model_feature_importance(self) -> pd.Series:
        importance = pd.Series(
            index=self.get_model_input_feature_names(),
            data=self.model.feature_importances_
        ).sort_values()
        return importance


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    nr = nameranking()
    nr.train()


    # test the gui:

    def gprint(txt, pos):
        text = font.render(txt, 1, (255, 255, 255))
        screen.blit(text, pos)


    def render(screen, n1, n2,
               hist: List[Tuple[str, str, str]],
               best_names: List[str],
               current_prediction: str):
        screen.fill((0, 0, 0))
        welcome = 'which one do you prefer?'
        gprint(welcome, (50, 5))
        names = f"{n1:>{width}} vs {n2:<{width}}"
        gprint(names, (5, 200))
        gprint(f"prediction: {current_prediction}", (50, 300))
        for i, (x1, x2, dr) in enumerate(reversed(hist[-10:])):
            f1 = int((1 - (i / 10)) * 255)
            f2 = int(f1 // 2)
            r = f2
            res = dr * 3

            nr1 = f"{x1:>{width}}"
            nr2 = f"{x2:<{width}}"
            x = font.render(res, 1, (r, r, r))
            screen.blit(x, (650, 40 + (1 + i) * 50))
            x = font.render(nr1, 1, (f1, f1, f1))
            screen.blit(x, (400, 40 + (1 + i) * 50))
            x = font.render(nr2, 1, (f2, f2, f2))
            screen.blit(x, (700, 40 + (1 + i) * 50))

        # best names are sorted with best coming last... so we reverse the order
        for i, name in enumerate(best_names[::-1]):
            x = font.render(name, 1, (255, 200, 200))
            screen.blit(x, (900, 40 + (1 + i) * 50))

        pygame.display.flip()


    import sys

    print(sys.argv)
    if len(sys.argv) > 1:
        gui = sys.argv[1] == "gui"
    else:
        gui = False

    if gui:
        width = 20

        os.environ['SDL_VIDEO_CENTERED'] = '1'
        pygame.init()
        clock = pygame.time.Clock()
        pygame.font.init()
        pygame.display.set_caption("Company Name Duel")
        font = pygame.font.SysFont('Hack', 20)
        fontsmall = pygame.font.SysFont('ubuntu', 15)
        screen = pygame.display.set_mode((1200, 600))

        # rename to "n_left, n_right"
        n1, n2 = nr.get_next_names()
        hist = []
        best_names: List[str] = []

        while True:
            duel_result = None
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
                    break
                    running = False
                elif event.type == pygame.KEYDOWN:
                    if (event.key == pygame.K_ESCAPE or
                            event.key == pygame.K_q):
                        pygame.quit()
                        continue
                    elif event.key == pygame.K_RIGHT:
                        if pygame.key.get_mods() & pygame.KMOD_SHIFT:
                            nr.insert_favorites(n2)
                            continue
                        else:
                            duel_result = ">"
                    elif event.key == pygame.K_LEFT:
                        if pygame.key.get_mods() & pygame.KMOD_SHIFT:
                            nr.insert_favorites(n1)
                            continue
                        else:
                            duel_result = "<"  # n1 is already the winning name
                    elif event.key == pygame.K_DOWN:
                        duel_result = "="
                    elif event.key == pygame.K_i:
                        namelist = nr.get_name_ranks(300, cached=False)
                        print(namelist)
                        winners = nr.count_word_outcomes()
                        print(winners)
                        feat_importance = nr.model_feature_importance()
                        print(feat_importance[feat_importance > 0.0])
                        continue
                    else:
                        pass

                    # gui logic only if relevant key was pressed
                    if duel_result:
                        duel = (n1, n2, duel_result)
                        hist += [duel]
                        nr.insert_duel_result(n1, n2, duel_result)
                        logger.info(f"duel result: {n1, duel_result, n2}")
                        n1, n2 = nr.get_next_names()
                        best_names = nr.get_name_ranks()

            # key = pygame.key.get_pressed()
            render(screen, n1, n2, hist, best_names, nr.predict_duel(n1, n2))
            clock.tick(60)

    if (sys.argv[1] == "g") if len(sys.argv) > 1 else False:
        all_names = nr.get_all_names()
        logger.info(f"number of evaluated names: {len(all_names)}")
        namelist = nr.get_name_ranks(sample_size=1000, cached=False)
        # test sorting:
        # the two following operations shosuld predict the exact opposite of each other
        best_name = namelist[-1]
        [nr.predict_duel(best_name, n) for n in namelist[:40]]
        [(best_name, n) for n in namelist[-40:]]
        [nr.predict_duel(n, best_name) for n in namelist[:40]]

        print(f"best names: {list(enumerate(namelist[-10:][::-1]))}")
        print(f"important features: {nr.model_feature_importance()}")
        print(f"some random names: {nr.generate_random_word(5000, 10)}")
    else:
        # debugging
        logger.info("debugging")
        nr.get_next_names()
