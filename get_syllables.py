from transformers import AutoTokenizer, AutoModel
import pandas as pd

tokenizer = AutoTokenizer.from_pretrained('distilbert-base-uncased')
model = AutoModel.from_pretrained('distilbert-base-uncased')

# save all embeddings in a pandas table
mbs = model.embeddings.word_embeddings.weight.detach().numpy()

# get all corresponding syllables from tokenizer

ids = list(range(len(mbs)))

a = tokenizer.convert_ids_to_tokens(ids)

df = pd.DataFrame(a, columns=["syl"])
df = df.loc[~df.syl.str.isnumeric()]
df['syl'] = df.syl.str.replace("#", "")
df = df.loc[df.syl.str.isalpha()]

tokenizer.convert_tokens_to_string(a)