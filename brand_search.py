import requests

url_base = 'http://www.wipo.int/branddb/en/?q='

query = {"searches": [
    {"te": "alpina", "fi": "BRAND"},
    {'te': 'CR', "fi": 'OO'},
    {'te': 'CO', "fi": 'HOLC'},
    {"te": "leche", "fi": "GS"}
]}

url = url_base + str(query).replace(" ", "").replace("'", "\"")
print(url)

response = requests.get(url)
print(response.status_code)

response.content
